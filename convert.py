import getopt, sys
import psycopg2
import psycopg2.extras

def log(s):
    sys.stderr.write(s)

def tagval(value, fields):
    '''Find the value for a single (key, value) pair, expanding $refernces
    as necessary'''
    if value[0] == '$':
        return fields[value[1:]]
    else:
        return value

def poptags1(pattern, fields):
    try:
        res = [(x[0], tagval(x[1], fields)) for x in pattern]
        return res
    except IndexError:
        sys.stderr.write('pattern=%s\n' % (pattern,))
        sys.stderr.write('fields=%s\n' % (fields,))
        raise

def poptags(pattern, fields):
    '''Resolve all the $name references in a (key, value) list 'pattern',
    using 'list' as the source for $values. Ignore pairs whose value end
    up being None.'''
    return [x for x in poptags1(pattern, fields) if x[1] is not None]

def splittags(s):
    '''Split a string of comma-separated key=value pairs into a list of
    (key, value) tuples'''
    s = s.strip()
    if not s: return []
    return [x.strip().split('=', 1) for x in s.split(',')]

def convert(conn, sink, schema, tablesuffix, controlfilename):
    '''Read the LMV tables and assign tags as directed by the control
    file, and call a 'sink' function for each feature'''
    lmcur = conn.cursor()
    recipe = open(controlfilename, 'r') 
    readahead = None
    while True:
        if readahead:
            head = readahead
            readahead = None
        else:
            head = recipe.readline()
        if not head: break

        head = head[:-1]
        items = head.split(None, 1)
        tableprefix = items[0]
        if len(items) > 1:
            rest = items[1]
            tags = splittags(rest)
        else:
            tags = []
        
        tablename = schema + '.' + tableprefix + tablesuffix

        test = ("SELECT tablename FROM pg_tables WHERE tablename ILIKE %s"
                " AND schemaname ILIKE %s")
        params = [ tableprefix + tablesuffix, schema ]
        lmcur.execute(test, params)
        testres = lmcur.fetchall()
        skip = not testres

        log('Processing table prefix: %s\n' % (tableprefix,))
        count = 0

        conditionals = {}
        
        while True:
            next = recipe.readline()
            if not next: break
            if not next[0].isspace():
                readahead = next
                break
            kkod, condrest = next.lstrip().split(':', 1)
            conditionals[kkod] = splittags(condrest)

        tcols = set([v[1][1:] for v in tags if v[1][0] == '$'])
        allcols = ['kkod', 'gid', 'g'] + list(tcols)
        cnames = ', '.join(allcols)

        missingkkod = set()

        sql = 'SELECT %(cnames)s FROM %(tablename)s' \
            % { 'cnames': cnames, 'tablename': tablename }

        if skip: continue

        lmcur.execute(sql)
        while True:
            row = lmcur.fetchone()
            if not row: break
            fields = { cn: str(row[k]) for k, cn in enumerate(allcols) }
            kkod = fields['kkod']
            try:
                ctags = conditionals[kkod]
            except KeyError:
                ctags = []
                missingkkod.add(kkod)

            tagvals = poptags(tags + ctags, fields)
            sink(fields['gid'], tableprefix, fields['kkod'], 
                 fields['g'], tagvals)
            count += 1
        
        log('Items processed: %d\n' % (count,))
        if missingkkod:
            mkk = ', '.join(map(str, missingkkod))
            log('Missing kkod:s: %s\n' % (mkk,))

    recipe.close()

cftab = '''CREATE TABLE IF NOT EXISTS lmvfeature (
    lmvtable TEXT,
    gid INTEGER,
    g GEOMETRY,
    tags HSTORE
)'''

cfflush = 'DELETE FROM lmvfeature'

cfinsert = '''INSERT INTO lmvfeature (lmvtable, gid, g, tags) VALUES (%s, %s, %s, %s)'''

class FeatureInsert:
    '''A simplistic "feature sink" that inserts all features as-is into
    a table'''
    def __init__(self, conn):
        self._cursor = conn.cursor()
        self._cursor.execute(cftab)
        self._cursor.execute(cfflush)

    def insert(self, gid, tableprefix, kkod, geom, tags):
        tdict = dict(tags)
        tdict['lmv:layer'] = tableprefix
        tdict['lmv:kkod'] = str(kkod)
        try:
            self._cursor.execute(cfinsert, [tableprefix, gid, geom, tdict])
        except:
            ety, eva, etr = sys.exc_info()
            log('Error inserting data,tableprefis=%s\n'
                'tags=%s\n'
                'exception=%s:%s\n'
                % (tableprefix, tdict, ety, eva))
            
            sys.exit(1)

def tagstr(tags):
    return ', '.join('%s=%s' % (k, v) for k, v in tags)

def main(argv):
    insert = False
    dbname = None
    opts, args = getopt.getopt(argv[1:], 'd:i')
    for opt, arg in opts:
        if opt == '-i':
            insert = True
        elif opt == '-d':
            dbname = arg

    if not dbname:
        raise Error('Must give -d DBNAME')

    conn = psycopg2.connect(dbname=dbname)

    # Install hstore <=> dict mapping
    psycopg2.extras.register_hstore(conn)

    if insert:
        feature = FeatureInsert(conn)
        sink = feature.insert
    else:
        feature = None
        sink = lambda gid, tableprefix, kkod, geom, tags:\
               sys.stdout.write('%s(%s): %s\n' % (tableprefix, gid, tagstr(tags)))

    schema = 'lmvtk'
    suffix = ''
    controlfile = 'terrangkartan.lmvosm'

    try:
        convert(conn, sink, schema, suffix, controlfile)
        conn.commit()
    except:
        conn.rollback()
        raise
    finally:
        conn.close()
    
if __name__ == '__main__':
    main(sys.argv)
